﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace OTUS.Delegates
{
	internal class FileViewer
	{
		public event EventHandler<FileArgs> FileFound;
		public event Action<string> FileViewerStopped;

		public async Task<string> Stroll(string path, CancellationToken cancellationToken)
		{
			string result = await Task.Run(() =>
			{
				DirectoryInfo directory;
				try
				{
					directory = new DirectoryInfo(path);
					foreach (var item in directory.GetFiles())
					{
						FileFound?.Invoke(this, new FileArgs(item.Name));
						if (cancellationToken.IsCancellationRequested)
						{
							return "Операция прервана токеном";
						}
						Task.Delay(500);
					}
					return null;
				}
				catch
				{
					return "Некорректный путь к каталогу";
				}
			}, cancellationToken);

			return result ?? "Поиск успешно завершен";
		}

		public string Stroll(string path, int ms)
		{
			var stopwatch = new Stopwatch();

			DirectoryInfo directory;
			try
			{
				directory = new DirectoryInfo(path);

				stopwatch.Start();
				foreach (var item in directory.GetFiles())
				{
					FileFound?.Invoke(this, new FileArgs(item.Name));
					if (stopwatch.ElapsedMilliseconds >= ms)
					{
						FileViewerStopped?.Invoke($"Поиск прерван событием по истечению {ms} мс");
						stopwatch.Stop();
						return string.Empty;
					}
				}
				stopwatch.Stop();
			}
			catch
			{
				return "Некорректный путь к каталогу";
			}

			return "Поиск успешно завершен";
		}
	}
}
