﻿using System;

namespace OTUS.Delegates
{
	internal class FileArgs : EventArgs
	{
		public FileArgs(string fileName)
		{
			FileName = fileName;
		}

		public string FileName { get; }
	}
}
