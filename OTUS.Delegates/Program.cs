﻿using OTUS.Delegates.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace OTUS.Delegates
{
	class Program
	{
		static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}

		private static async Task MainAsync()
		{
			Person[] persons = new Person[] { new Person { Name = "Ефросинья", Height = 40.04f },
											  new Person { Name = "Максимилиан", Height = 30.03f },
											  new Person { Name = "Мария", Height = 20.02f },
											  new Person { Name = "Тимофей", Height = 50.05f },
											  new Person { Name = "Иля", Height = 10.01f }};

			var personsCollection = new List<Person>(persons);
			Person personWithMaxHeight = personsCollection.GetMax(x => (x?.Height ?? 0f));
			Console.WriteLine($"Элемент с максимальным значением: " +
				$"Name = {personWithMaxHeight.Name} " +
				$"Height = {personWithMaxHeight.Height}");


			Console.WriteLine("Delegates!");
			string testPath = Path.Combine("D:\\");
			First(testPath);

			await Second(testPath);

			Console.ReadLine();
		}

		private static void First(string testPath)
		{
			var viewer = new FileViewer();
			viewer.FileFound += OnFileFound;
			viewer.FileViewerStopped += OnFileViewerStopped;
			var result = viewer.Stroll(testPath, 200);
			Console.WriteLine(result);

			viewer.FileViewerStopped -= OnFileViewerStopped;
			viewer.FileFound -= OnFileFound;
		}

		private static async Task Second(string testPath)
		{
			var cancelTokenSource = new CancellationTokenSource();
			var token = cancelTokenSource.Token;
			var viewer = new FileViewer();
			viewer.FileFound += OnFileFound;

			try
			{
				var result = viewer.Stroll(testPath, token); //Без async, чтобы вызвать отмену задачи
				cancelTokenSource.Cancel();
				await Task.Delay(500);
				Console.WriteLine(result.Result);
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
			}

			viewer.FileFound -= OnFileFound;

		}

		private static void OnFileFound(object sender, FileArgs e)
		{
			Console.WriteLine(e.FileName);
		}

		private static void OnFileViewerStopped(string message)
		{
			Console.WriteLine(message);
		}

	}
}
