﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OTUS.Delegates.Helpers
{
	internal static class EnumerableMaxHelper
	{
		public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> getValueFunc) 
			where T: class
		{
			var max = collection.Max(getValueFunc);
			return collection.FirstOrDefault(i => getValueFunc(i) == max);
			//return collection.OrderByDescending(getValueFunc).FirstOrDefault();
		}
	}
}
